package com.kapparhopi.grapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A OpenApi.
 */
@Entity
@Table(name = "open_api")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OpenApi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "version")
    private String version;

    @NotNull
    @Column(name = "uri", nullable = false)
    private String uri;

    @OneToMany(mappedBy = "openApi")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Path> paths = new HashSet<>();
    @ManyToOne
    @JsonIgnoreProperties("")
    private User owner;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public OpenApi title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public OpenApi version(String version) {
        this.version = version;
        return this;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUri() {
        return uri;
    }

    public OpenApi uri(String uri) {
        this.uri = uri;
        return this;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Set<Path> getPaths() {
        return paths;
    }

    public OpenApi paths(Set<Path> paths) {
        this.paths = paths;
        return this;
    }

    public OpenApi addPaths(Path path) {
        this.paths.add(path);
        path.setOpenApi(this);
        return this;
    }

    public OpenApi removePaths(Path path) {
        this.paths.remove(path);
        path.setOpenApi(null);
        return this;
    }

    public void setPaths(Set<Path> paths) {
        this.paths = paths;
    }

    public User getOwner() {
        return owner;
    }

    public OpenApi owner(User user) {
        this.owner = user;
        return this;
    }

    public void setOwner(User user) {
        this.owner = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        OpenApi openApi = (OpenApi) o;
        if (openApi.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), openApi.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OpenApi{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", version='" + getVersion() + "'" +
            ", uri='" + getUri() + "'" +
            "}";
    }
}
