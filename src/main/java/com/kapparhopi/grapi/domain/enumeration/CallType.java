package com.kapparhopi.grapi.domain.enumeration;

/**
 * The CallType enumeration.
 */
public enum CallType {
    GET, PUT, POST, PATCH, DELETE
}
