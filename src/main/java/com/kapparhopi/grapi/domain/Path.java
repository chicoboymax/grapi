package com.kapparhopi.grapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

import com.kapparhopi.grapi.domain.enumeration.CallType;

/**
 * A Path.
 */
@Entity
@Table(name = "path")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Path implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "jhi_type", nullable = false)
    private CallType type;

    @NotNull
    @Column(name = "path", nullable = false)
    private String path;

    @ManyToOne
    @JsonIgnoreProperties("paths")
    private OpenApi openApi;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CallType getType() {
        return type;
    }

    public Path type(CallType type) {
        this.type = type;
        return this;
    }

    public void setType(CallType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public Path path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public OpenApi getOpenApi() {
        return openApi;
    }

    public Path openApi(OpenApi openApi) {
        this.openApi = openApi;
        return this;
    }

    public void setOpenApi(OpenApi openApi) {
        this.openApi = openApi;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Path path = (Path) o;
        if (path.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), path.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Path{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", path='" + getPath() + "'" +
            "}";
    }
}
