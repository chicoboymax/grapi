/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kapparhopi.grapi.web.rest.vm;
