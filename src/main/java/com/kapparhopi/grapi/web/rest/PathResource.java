package com.kapparhopi.grapi.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kapparhopi.grapi.service.PathService;
import com.kapparhopi.grapi.web.rest.errors.BadRequestAlertException;
import com.kapparhopi.grapi.web.rest.util.HeaderUtil;
import com.kapparhopi.grapi.service.dto.PathDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Path.
 */
@RestController
@RequestMapping("/api")
public class PathResource {

    private final Logger log = LoggerFactory.getLogger(PathResource.class);

    private static final String ENTITY_NAME = "path";

    private final PathService pathService;

    public PathResource(PathService pathService) {
        this.pathService = pathService;
    }

    /**
     * POST  /paths : Create a new path.
     *
     * @param pathDTO the pathDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pathDTO, or with status 400 (Bad Request) if the path has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/paths")
    @Timed
    public ResponseEntity<PathDTO> createPath(@Valid @RequestBody PathDTO pathDTO) throws URISyntaxException {
        log.debug("REST request to save Path : {}", pathDTO);
        if (pathDTO.getId() != null) {
            throw new BadRequestAlertException("A new path cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PathDTO result = pathService.save(pathDTO);
        return ResponseEntity.created(new URI("/api/paths/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /paths : Updates an existing path.
     *
     * @param pathDTO the pathDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pathDTO,
     * or with status 400 (Bad Request) if the pathDTO is not valid,
     * or with status 500 (Internal Server Error) if the pathDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/paths")
    @Timed
    public ResponseEntity<PathDTO> updatePath(@Valid @RequestBody PathDTO pathDTO) throws URISyntaxException {
        log.debug("REST request to update Path : {}", pathDTO);
        if (pathDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PathDTO result = pathService.save(pathDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pathDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /paths : get all the paths.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of paths in body
     */
    @GetMapping("/paths")
    @Timed
    public List<PathDTO> getAllPaths() {
        log.debug("REST request to get all Paths");
        return pathService.findAll();
    }

    /**
     * GET  /paths/:id : get the "id" path.
     *
     * @param id the id of the pathDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pathDTO, or with status 404 (Not Found)
     */
    @GetMapping("/paths/{id}")
    @Timed
    public ResponseEntity<PathDTO> getPath(@PathVariable Long id) {
        log.debug("REST request to get Path : {}", id);
        Optional<PathDTO> pathDTO = pathService.findOne(id);
        return ResponseUtil.wrapOrNotFound(pathDTO);
    }

    /**
     * DELETE  /paths/:id : delete the "id" path.
     *
     * @param id the id of the pathDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/paths/{id}")
    @Timed
    public ResponseEntity<Void> deletePath(@PathVariable Long id) {
        log.debug("REST request to delete Path : {}", id);
        pathService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
