package com.kapparhopi.grapi.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.kapparhopi.grapi.service.OpenApiService;
import com.kapparhopi.grapi.web.rest.errors.BadRequestAlertException;
import com.kapparhopi.grapi.web.rest.util.HeaderUtil;
import com.kapparhopi.grapi.service.dto.OpenApiDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing OpenApi.
 */
@RestController
@RequestMapping("/api")
public class OpenApiResource {

    private final Logger log = LoggerFactory.getLogger(OpenApiResource.class);

    private static final String ENTITY_NAME = "openApi";

    private final OpenApiService openApiService;

    public OpenApiResource(OpenApiService openApiService) {
        this.openApiService = openApiService;
    }

    /**
     * POST  /open-apis : Create a new openApi.
     *
     * @param openApiDTO the openApiDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new openApiDTO, or with status 400 (Bad Request) if the openApi has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/open-apis")
    @Timed
    public ResponseEntity<OpenApiDTO> createOpenApi(@Valid @RequestBody OpenApiDTO openApiDTO) throws URISyntaxException {
        log.debug("REST request to save OpenApi : {}", openApiDTO);
        if (openApiDTO.getId() != null) {
            throw new BadRequestAlertException("A new openApi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OpenApiDTO result = openApiService.save(openApiDTO);
        return ResponseEntity.created(new URI("/api/open-apis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /open-apis : Updates an existing openApi.
     *
     * @param openApiDTO the openApiDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated openApiDTO,
     * or with status 400 (Bad Request) if the openApiDTO is not valid,
     * or with status 500 (Internal Server Error) if the openApiDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/open-apis")
    @Timed
    public ResponseEntity<OpenApiDTO> updateOpenApi(@Valid @RequestBody OpenApiDTO openApiDTO) throws URISyntaxException {
        log.debug("REST request to update OpenApi : {}", openApiDTO);
        if (openApiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OpenApiDTO result = openApiService.save(openApiDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, openApiDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /open-apis : get all the openApis.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of openApis in body
     */
    @GetMapping("/open-apis")
    @Timed
    public List<OpenApiDTO> getAllOpenApis() {
        log.debug("REST request to get all OpenApis");
        return openApiService.findAll();
    }

    /**
     * GET  /open-apis/:id : get the "id" openApi.
     *
     * @param id the id of the openApiDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the openApiDTO, or with status 404 (Not Found)
     */
    @GetMapping("/open-apis/{id}")
    @Timed
    public ResponseEntity<OpenApiDTO> getOpenApi(@PathVariable Long id) {
        log.debug("REST request to get OpenApi : {}", id);
        Optional<OpenApiDTO> openApiDTO = openApiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(openApiDTO);
    }

    /**
     * DELETE  /open-apis/:id : delete the "id" openApi.
     *
     * @param id the id of the openApiDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/open-apis/{id}")
    @Timed
    public ResponseEntity<Void> deleteOpenApi(@PathVariable Long id) {
        log.debug("REST request to delete OpenApi : {}", id);
        openApiService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
