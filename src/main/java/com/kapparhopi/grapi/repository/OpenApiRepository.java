package com.kapparhopi.grapi.repository;

import com.kapparhopi.grapi.domain.OpenApi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the OpenApi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OpenApiRepository extends JpaRepository<OpenApi, Long> {

    @Query("select open_api from OpenApi open_api where open_api.owner.login = ?#{principal.username}")
    List<OpenApi> findByOwnerIsCurrentUser();

}
