package com.kapparhopi.grapi.service;

import com.kapparhopi.grapi.service.dto.OpenApiDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing OpenApi.
 */
public interface OpenApiService {

    /**
     * Save a openApi.
     *
     * @param openApiDTO the entity to save
     * @return the persisted entity
     */
    OpenApiDTO save(OpenApiDTO openApiDTO);

    /**
     * Get all the openApis.
     *
     * @return the list of entities
     */
    List<OpenApiDTO> findAll();


    /**
     * Get the "id" openApi.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<OpenApiDTO> findOne(Long id);

    /**
     * Delete the "id" openApi.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
