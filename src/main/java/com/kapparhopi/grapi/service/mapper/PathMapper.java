package com.kapparhopi.grapi.service.mapper;

import com.kapparhopi.grapi.domain.*;
import com.kapparhopi.grapi.service.dto.PathDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Path and its DTO PathDTO.
 */
@Mapper(componentModel = "spring", uses = {OpenApiMapper.class})
public interface PathMapper extends EntityMapper<PathDTO, Path> {

    @Mapping(source = "openApi.id", target = "openApiId")
    PathDTO toDto(Path path);

    @Mapping(source = "openApiId", target = "openApi")
    Path toEntity(PathDTO pathDTO);

    default Path fromId(Long id) {
        if (id == null) {
            return null;
        }
        Path path = new Path();
        path.setId(id);
        return path;
    }
}
