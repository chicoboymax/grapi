package com.kapparhopi.grapi.service.mapper;

import com.kapparhopi.grapi.domain.*;
import com.kapparhopi.grapi.service.dto.OpenApiDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity OpenApi and its DTO OpenApiDTO.
 */
@Mapper(componentModel = "spring", uses = {UserMapper.class})
public interface OpenApiMapper extends EntityMapper<OpenApiDTO, OpenApi> {

    @Mapping(source = "owner.id", target = "ownerId")
    OpenApiDTO toDto(OpenApi openApi);

    @Mapping(target = "paths", ignore = true)
    @Mapping(source = "ownerId", target = "owner")
    OpenApi toEntity(OpenApiDTO openApiDTO);

    default OpenApi fromId(Long id) {
        if (id == null) {
            return null;
        }
        OpenApi openApi = new OpenApi();
        openApi.setId(id);
        return openApi;
    }
}
