package com.kapparhopi.grapi.service.impl;

import com.kapparhopi.grapi.service.PathService;
import com.kapparhopi.grapi.domain.Path;
import com.kapparhopi.grapi.repository.PathRepository;
import com.kapparhopi.grapi.service.dto.PathDTO;
import com.kapparhopi.grapi.service.mapper.PathMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Path.
 */
@Service
@Transactional
public class PathServiceImpl implements PathService {

    private final Logger log = LoggerFactory.getLogger(PathServiceImpl.class);

    private final PathRepository pathRepository;

    private final PathMapper pathMapper;

    public PathServiceImpl(PathRepository pathRepository, PathMapper pathMapper) {
        this.pathRepository = pathRepository;
        this.pathMapper = pathMapper;
    }

    /**
     * Save a path.
     *
     * @param pathDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PathDTO save(PathDTO pathDTO) {
        log.debug("Request to save Path : {}", pathDTO);

        Path path = pathMapper.toEntity(pathDTO);
        path = pathRepository.save(path);
        return pathMapper.toDto(path);
    }

    /**
     * Get all the paths.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<PathDTO> findAll() {
        log.debug("Request to get all Paths");
        return pathRepository.findAll().stream()
            .map(pathMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one path by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<PathDTO> findOne(Long id) {
        log.debug("Request to get Path : {}", id);
        return pathRepository.findById(id)
            .map(pathMapper::toDto);
    }

    /**
     * Delete the path by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Path : {}", id);
        pathRepository.deleteById(id);
    }
}
