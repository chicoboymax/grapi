package com.kapparhopi.grapi.service.impl;

import com.kapparhopi.grapi.service.OpenApiService;
import com.kapparhopi.grapi.domain.OpenApi;
import com.kapparhopi.grapi.repository.OpenApiRepository;
import com.kapparhopi.grapi.service.dto.OpenApiDTO;
import com.kapparhopi.grapi.service.mapper.OpenApiMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing OpenApi.
 */
@Service
@Transactional
public class OpenApiServiceImpl implements OpenApiService {

    private final Logger log = LoggerFactory.getLogger(OpenApiServiceImpl.class);

    private final OpenApiRepository openApiRepository;

    private final OpenApiMapper openApiMapper;

    public OpenApiServiceImpl(OpenApiRepository openApiRepository, OpenApiMapper openApiMapper) {
        this.openApiRepository = openApiRepository;
        this.openApiMapper = openApiMapper;
    }

    /**
     * Save a openApi.
     *
     * @param openApiDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public OpenApiDTO save(OpenApiDTO openApiDTO) {
        log.debug("Request to save OpenApi : {}", openApiDTO);

        OpenApi openApi = openApiMapper.toEntity(openApiDTO);
        openApi = openApiRepository.save(openApi);
        return openApiMapper.toDto(openApi);
    }

    /**
     * Get all the openApis.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<OpenApiDTO> findAll() {
        log.debug("Request to get all OpenApis");
        return openApiRepository.findAll().stream()
            .map(openApiMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     * Get one openApi by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<OpenApiDTO> findOne(Long id) {
        log.debug("Request to get OpenApi : {}", id);
        return openApiRepository.findById(id)
            .map(openApiMapper::toDto);
    }

    /**
     * Delete the openApi by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete OpenApi : {}", id);
        openApiRepository.deleteById(id);
    }
}
