package com.kapparhopi.grapi.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the OpenApi entity.
 */
public class OpenApiDTO implements Serializable {

    private Long id;

    @NotNull
    private String title;

    private String version;

    @NotNull
    private String uri;

    private Long ownerId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long userId) {
        this.ownerId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OpenApiDTO openApiDTO = (OpenApiDTO) o;
        if (openApiDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), openApiDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "OpenApiDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", version='" + getVersion() + "'" +
            ", uri='" + getUri() + "'" +
            ", owner=" + getOwnerId() +
            "}";
    }
}
