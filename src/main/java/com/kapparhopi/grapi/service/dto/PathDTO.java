package com.kapparhopi.grapi.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.kapparhopi.grapi.domain.enumeration.CallType;

/**
 * A DTO for the Path entity.
 */
public class PathDTO implements Serializable {

    private Long id;

    @NotNull
    private CallType type;

    @NotNull
    private String path;

    private Long openApiId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CallType getType() {
        return type;
    }

    public void setType(CallType type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getOpenApiId() {
        return openApiId;
    }

    public void setOpenApiId(Long openApiId) {
        this.openApiId = openApiId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PathDTO pathDTO = (PathDTO) o;
        if (pathDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), pathDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PathDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", path='" + getPath() + "'" +
            ", openApi=" + getOpenApiId() +
            "}";
    }
}
