package com.kapparhopi.grapi.service;

import com.kapparhopi.grapi.service.dto.PathDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Path.
 */
public interface PathService {

    /**
     * Save a path.
     *
     * @param pathDTO the entity to save
     * @return the persisted entity
     */
    PathDTO save(PathDTO pathDTO);

    /**
     * Get all the paths.
     *
     * @return the list of entities
     */
    List<PathDTO> findAll();


    /**
     * Get the "id" path.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<PathDTO> findOne(Long id);

    /**
     * Delete the "id" path.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
