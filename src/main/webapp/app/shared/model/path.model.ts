export const enum CallType {
  GET = 'GET',
  PUT = 'PUT',
  POST = 'POST',
  PATCH = 'PATCH',
  DELETE = 'DELETE'
}

export interface IPath {
  id?: number;
  type?: CallType;
  path?: string;
  openApiId?: number;
}

export const defaultValue: Readonly<IPath> = {};
