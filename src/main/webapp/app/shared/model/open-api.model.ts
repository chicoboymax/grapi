import { IPath } from 'app/shared/model//path.model';

export interface IOpenApi {
  id?: number;
  title?: string;
  version?: string;
  uri?: string;
  paths?: IPath[];
  ownerId?: number;
}

export const defaultValue: Readonly<IOpenApi> = {};
