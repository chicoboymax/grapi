import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IOpenApi } from 'app/shared/model/open-api.model';
import { getEntities as getOpenApis } from 'app/entities/open-api/open-api.reducer';
import { getEntity, updateEntity, createEntity, reset } from './path.reducer';
import { IPath } from 'app/shared/model/path.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IPathUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IPathUpdateState {
  isNew: boolean;
  openApiId: string;
}

export class PathUpdate extends React.Component<IPathUpdateProps, IPathUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      openApiId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getOpenApis();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { pathEntity } = this.props;
      const entity = {
        ...pathEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/path');
  };

  render() {
    const { pathEntity, openApis, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="grapiApp.path.home.createOrEditLabel">
              <Translate contentKey="grapiApp.path.home.createOrEditLabel">Create or edit a Path</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : pathEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="path-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="typeLabel">
                    <Translate contentKey="grapiApp.path.type">Type</Translate>
                  </Label>
                  <AvInput id="path-type" type="select" className="form-control" name="type" value={(!isNew && pathEntity.type) || 'GET'}>
                    <option value="GET">
                      <Translate contentKey="grapiApp.CallType.GET" />
                    </option>
                    <option value="PUT">
                      <Translate contentKey="grapiApp.CallType.PUT" />
                    </option>
                    <option value="POST">
                      <Translate contentKey="grapiApp.CallType.POST" />
                    </option>
                    <option value="PATCH">
                      <Translate contentKey="grapiApp.CallType.PATCH" />
                    </option>
                    <option value="DELETE">
                      <Translate contentKey="grapiApp.CallType.DELETE" />
                    </option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="pathLabel" for="path">
                    <Translate contentKey="grapiApp.path.path">Path</Translate>
                  </Label>
                  <AvField
                    id="path-path"
                    type="text"
                    name="path"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="openApi.id">
                    <Translate contentKey="grapiApp.path.openApi">Open Api</Translate>
                  </Label>
                  <AvInput id="path-openApi" type="select" className="form-control" name="openApiId">
                    <option value="" key="0" />
                    {openApis
                      ? openApis.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/path" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  openApis: storeState.openApi.entities,
  pathEntity: storeState.path.entity,
  loading: storeState.path.loading,
  updating: storeState.path.updating,
  updateSuccess: storeState.path.updateSuccess
});

const mapDispatchToProps = {
  getOpenApis,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PathUpdate);
