import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IPath, defaultValue } from 'app/shared/model/path.model';

export const ACTION_TYPES = {
  FETCH_PATH_LIST: 'path/FETCH_PATH_LIST',
  FETCH_PATH: 'path/FETCH_PATH',
  CREATE_PATH: 'path/CREATE_PATH',
  UPDATE_PATH: 'path/UPDATE_PATH',
  DELETE_PATH: 'path/DELETE_PATH',
  RESET: 'path/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IPath>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type PathState = Readonly<typeof initialState>;

// Reducer

export default (state: PathState = initialState, action): PathState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PATH_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PATH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_PATH):
    case REQUEST(ACTION_TYPES.UPDATE_PATH):
    case REQUEST(ACTION_TYPES.DELETE_PATH):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_PATH_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PATH):
    case FAILURE(ACTION_TYPES.CREATE_PATH):
    case FAILURE(ACTION_TYPES.UPDATE_PATH):
    case FAILURE(ACTION_TYPES.DELETE_PATH):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_PATH_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_PATH):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_PATH):
    case SUCCESS(ACTION_TYPES.UPDATE_PATH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_PATH):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/paths';

// Actions

export const getEntities: ICrudGetAllAction<IPath> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_PATH_LIST,
  payload: axios.get<IPath>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IPath> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PATH,
    payload: axios.get<IPath>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IPath> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PATH,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IPath> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PATH,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IPath> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PATH,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
