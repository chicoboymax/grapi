import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './path.reducer';
import { IPath } from 'app/shared/model/path.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPathDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class PathDetail extends React.Component<IPathDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { pathEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="grapiApp.path.detail.title">Path</Translate> [<b>{pathEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="type">
                <Translate contentKey="grapiApp.path.type">Type</Translate>
              </span>
            </dt>
            <dd>{pathEntity.type}</dd>
            <dt>
              <span id="path">
                <Translate contentKey="grapiApp.path.path">Path</Translate>
              </span>
            </dt>
            <dd>{pathEntity.path}</dd>
            <dt>
              <Translate contentKey="grapiApp.path.openApi">Open Api</Translate>
            </dt>
            <dd>{pathEntity.openApiId ? pathEntity.openApiId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/path" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/path/${pathEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ path }: IRootState) => ({
  pathEntity: path.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PathDetail);
