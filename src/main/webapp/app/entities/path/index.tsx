import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Path from './path';
import PathDetail from './path-detail';
import PathUpdate from './path-update';
import PathDeleteDialog from './path-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={PathUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={PathUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={PathDetail} />
      <ErrorBoundaryRoute path={match.url} component={Path} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={PathDeleteDialog} />
  </>
);

export default Routes;
