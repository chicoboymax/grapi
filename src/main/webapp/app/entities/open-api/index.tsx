import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import OpenApi from './open-api';
import OpenApiDetail from './open-api-detail';
import OpenApiUpdate from './open-api-update';
import OpenApiDeleteDialog from './open-api-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={OpenApiUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={OpenApiUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={OpenApiDetail} />
      <ErrorBoundaryRoute path={match.url} component={OpenApi} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={OpenApiDeleteDialog} />
  </>
);

export default Routes;
