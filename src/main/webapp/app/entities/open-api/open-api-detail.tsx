import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './open-api.reducer';
import { IOpenApi } from 'app/shared/model/open-api.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOpenApiDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class OpenApiDetail extends React.Component<IOpenApiDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { openApiEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="grapiApp.openApi.detail.title">OpenApi</Translate> [<b>{openApiEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="title">
                <Translate contentKey="grapiApp.openApi.title">Title</Translate>
              </span>
            </dt>
            <dd>{openApiEntity.title}</dd>
            <dt>
              <span id="version">
                <Translate contentKey="grapiApp.openApi.version">Version</Translate>
              </span>
            </dt>
            <dd>{openApiEntity.version}</dd>
            <dt>
              <span id="uri">
                <Translate contentKey="grapiApp.openApi.uri">Uri</Translate>
              </span>
            </dt>
            <dd>{openApiEntity.uri}</dd>
            <dt>
              <Translate contentKey="grapiApp.openApi.owner">Owner</Translate>
            </dt>
            <dd>{openApiEntity.ownerId ? openApiEntity.ownerId : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/open-api" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/open-api/${openApiEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ openApi }: IRootState) => ({
  openApiEntity: openApi.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OpenApiDetail);
