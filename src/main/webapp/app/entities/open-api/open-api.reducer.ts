import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IOpenApi, defaultValue } from 'app/shared/model/open-api.model';

export const ACTION_TYPES = {
  FETCH_OPENAPI_LIST: 'openApi/FETCH_OPENAPI_LIST',
  FETCH_OPENAPI: 'openApi/FETCH_OPENAPI',
  CREATE_OPENAPI: 'openApi/CREATE_OPENAPI',
  UPDATE_OPENAPI: 'openApi/UPDATE_OPENAPI',
  DELETE_OPENAPI: 'openApi/DELETE_OPENAPI',
  RESET: 'openApi/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IOpenApi>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type OpenApiState = Readonly<typeof initialState>;

// Reducer

export default (state: OpenApiState = initialState, action): OpenApiState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_OPENAPI_LIST):
    case REQUEST(ACTION_TYPES.FETCH_OPENAPI):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_OPENAPI):
    case REQUEST(ACTION_TYPES.UPDATE_OPENAPI):
    case REQUEST(ACTION_TYPES.DELETE_OPENAPI):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_OPENAPI_LIST):
    case FAILURE(ACTION_TYPES.FETCH_OPENAPI):
    case FAILURE(ACTION_TYPES.CREATE_OPENAPI):
    case FAILURE(ACTION_TYPES.UPDATE_OPENAPI):
    case FAILURE(ACTION_TYPES.DELETE_OPENAPI):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_OPENAPI_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_OPENAPI):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_OPENAPI):
    case SUCCESS(ACTION_TYPES.UPDATE_OPENAPI):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_OPENAPI):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/open-apis';

// Actions

export const getEntities: ICrudGetAllAction<IOpenApi> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_OPENAPI_LIST,
  payload: axios.get<IOpenApi>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IOpenApi> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_OPENAPI,
    payload: axios.get<IOpenApi>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IOpenApi> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_OPENAPI,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IOpenApi> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_OPENAPI,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IOpenApi> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_OPENAPI,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
