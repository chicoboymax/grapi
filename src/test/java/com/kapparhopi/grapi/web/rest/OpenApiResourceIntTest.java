package com.kapparhopi.grapi.web.rest;

import com.kapparhopi.grapi.GrapiApp;

import com.kapparhopi.grapi.domain.OpenApi;
import com.kapparhopi.grapi.repository.OpenApiRepository;
import com.kapparhopi.grapi.service.OpenApiService;
import com.kapparhopi.grapi.service.dto.OpenApiDTO;
import com.kapparhopi.grapi.service.mapper.OpenApiMapper;
import com.kapparhopi.grapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.kapparhopi.grapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the OpenApiResource REST controller.
 *
 * @see OpenApiResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GrapiApp.class)
public class OpenApiResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_VERSION = "AAAAAAAAAA";
    private static final String UPDATED_VERSION = "BBBBBBBBBB";

    private static final String DEFAULT_URI = "AAAAAAAAAA";
    private static final String UPDATED_URI = "BBBBBBBBBB";

    @Autowired
    private OpenApiRepository openApiRepository;

    @Autowired
    private OpenApiMapper openApiMapper;

    @Autowired
    private OpenApiService openApiService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOpenApiMockMvc;

    private OpenApi openApi;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OpenApiResource openApiResource = new OpenApiResource(openApiService);
        this.restOpenApiMockMvc = MockMvcBuilders.standaloneSetup(openApiResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OpenApi createEntity(EntityManager em) {
        OpenApi openApi = new OpenApi()
            .title(DEFAULT_TITLE)
            .version(DEFAULT_VERSION)
            .uri(DEFAULT_URI);
        return openApi;
    }

    @Before
    public void initTest() {
        openApi = createEntity(em);
    }

    @Test
    @Transactional
    public void createOpenApi() throws Exception {
        int databaseSizeBeforeCreate = openApiRepository.findAll().size();

        // Create the OpenApi
        OpenApiDTO openApiDTO = openApiMapper.toDto(openApi);
        restOpenApiMockMvc.perform(post("/api/open-apis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(openApiDTO)))
            .andExpect(status().isCreated());

        // Validate the OpenApi in the database
        List<OpenApi> openApiList = openApiRepository.findAll();
        assertThat(openApiList).hasSize(databaseSizeBeforeCreate + 1);
        OpenApi testOpenApi = openApiList.get(openApiList.size() - 1);
        assertThat(testOpenApi.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testOpenApi.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testOpenApi.getUri()).isEqualTo(DEFAULT_URI);
    }

    @Test
    @Transactional
    public void createOpenApiWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = openApiRepository.findAll().size();

        // Create the OpenApi with an existing ID
        openApi.setId(1L);
        OpenApiDTO openApiDTO = openApiMapper.toDto(openApi);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOpenApiMockMvc.perform(post("/api/open-apis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(openApiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OpenApi in the database
        List<OpenApi> openApiList = openApiRepository.findAll();
        assertThat(openApiList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = openApiRepository.findAll().size();
        // set the field null
        openApi.setTitle(null);

        // Create the OpenApi, which fails.
        OpenApiDTO openApiDTO = openApiMapper.toDto(openApi);

        restOpenApiMockMvc.perform(post("/api/open-apis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(openApiDTO)))
            .andExpect(status().isBadRequest());

        List<OpenApi> openApiList = openApiRepository.findAll();
        assertThat(openApiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUriIsRequired() throws Exception {
        int databaseSizeBeforeTest = openApiRepository.findAll().size();
        // set the field null
        openApi.setUri(null);

        // Create the OpenApi, which fails.
        OpenApiDTO openApiDTO = openApiMapper.toDto(openApi);

        restOpenApiMockMvc.perform(post("/api/open-apis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(openApiDTO)))
            .andExpect(status().isBadRequest());

        List<OpenApi> openApiList = openApiRepository.findAll();
        assertThat(openApiList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllOpenApis() throws Exception {
        // Initialize the database
        openApiRepository.saveAndFlush(openApi);

        // Get all the openApiList
        restOpenApiMockMvc.perform(get("/api/open-apis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(openApi.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.toString())))
            .andExpect(jsonPath("$.[*].uri").value(hasItem(DEFAULT_URI.toString())));
    }
    
    @Test
    @Transactional
    public void getOpenApi() throws Exception {
        // Initialize the database
        openApiRepository.saveAndFlush(openApi);

        // Get the openApi
        restOpenApiMockMvc.perform(get("/api/open-apis/{id}", openApi.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(openApi.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.toString()))
            .andExpect(jsonPath("$.uri").value(DEFAULT_URI.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOpenApi() throws Exception {
        // Get the openApi
        restOpenApiMockMvc.perform(get("/api/open-apis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOpenApi() throws Exception {
        // Initialize the database
        openApiRepository.saveAndFlush(openApi);

        int databaseSizeBeforeUpdate = openApiRepository.findAll().size();

        // Update the openApi
        OpenApi updatedOpenApi = openApiRepository.findById(openApi.getId()).get();
        // Disconnect from session so that the updates on updatedOpenApi are not directly saved in db
        em.detach(updatedOpenApi);
        updatedOpenApi
            .title(UPDATED_TITLE)
            .version(UPDATED_VERSION)
            .uri(UPDATED_URI);
        OpenApiDTO openApiDTO = openApiMapper.toDto(updatedOpenApi);

        restOpenApiMockMvc.perform(put("/api/open-apis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(openApiDTO)))
            .andExpect(status().isOk());

        // Validate the OpenApi in the database
        List<OpenApi> openApiList = openApiRepository.findAll();
        assertThat(openApiList).hasSize(databaseSizeBeforeUpdate);
        OpenApi testOpenApi = openApiList.get(openApiList.size() - 1);
        assertThat(testOpenApi.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testOpenApi.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testOpenApi.getUri()).isEqualTo(UPDATED_URI);
    }

    @Test
    @Transactional
    public void updateNonExistingOpenApi() throws Exception {
        int databaseSizeBeforeUpdate = openApiRepository.findAll().size();

        // Create the OpenApi
        OpenApiDTO openApiDTO = openApiMapper.toDto(openApi);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOpenApiMockMvc.perform(put("/api/open-apis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(openApiDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OpenApi in the database
        List<OpenApi> openApiList = openApiRepository.findAll();
        assertThat(openApiList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOpenApi() throws Exception {
        // Initialize the database
        openApiRepository.saveAndFlush(openApi);

        int databaseSizeBeforeDelete = openApiRepository.findAll().size();

        // Get the openApi
        restOpenApiMockMvc.perform(delete("/api/open-apis/{id}", openApi.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<OpenApi> openApiList = openApiRepository.findAll();
        assertThat(openApiList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OpenApi.class);
        OpenApi openApi1 = new OpenApi();
        openApi1.setId(1L);
        OpenApi openApi2 = new OpenApi();
        openApi2.setId(openApi1.getId());
        assertThat(openApi1).isEqualTo(openApi2);
        openApi2.setId(2L);
        assertThat(openApi1).isNotEqualTo(openApi2);
        openApi1.setId(null);
        assertThat(openApi1).isNotEqualTo(openApi2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OpenApiDTO.class);
        OpenApiDTO openApiDTO1 = new OpenApiDTO();
        openApiDTO1.setId(1L);
        OpenApiDTO openApiDTO2 = new OpenApiDTO();
        assertThat(openApiDTO1).isNotEqualTo(openApiDTO2);
        openApiDTO2.setId(openApiDTO1.getId());
        assertThat(openApiDTO1).isEqualTo(openApiDTO2);
        openApiDTO2.setId(2L);
        assertThat(openApiDTO1).isNotEqualTo(openApiDTO2);
        openApiDTO1.setId(null);
        assertThat(openApiDTO1).isNotEqualTo(openApiDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(openApiMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(openApiMapper.fromId(null)).isNull();
    }
}
