package com.kapparhopi.grapi.web.rest;

import com.kapparhopi.grapi.GrapiApp;

import com.kapparhopi.grapi.domain.Path;
import com.kapparhopi.grapi.repository.PathRepository;
import com.kapparhopi.grapi.service.PathService;
import com.kapparhopi.grapi.service.dto.PathDTO;
import com.kapparhopi.grapi.service.mapper.PathMapper;
import com.kapparhopi.grapi.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static com.kapparhopi.grapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.kapparhopi.grapi.domain.enumeration.CallType;
/**
 * Test class for the PathResource REST controller.
 *
 * @see PathResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GrapiApp.class)
public class PathResourceIntTest {

    private static final CallType DEFAULT_TYPE = CallType.GET;
    private static final CallType UPDATED_TYPE = CallType.PUT;

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    @Autowired
    private PathRepository pathRepository;

    @Autowired
    private PathMapper pathMapper;

    @Autowired
    private PathService pathService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPathMockMvc;

    private Path path;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PathResource pathResource = new PathResource(pathService);
        this.restPathMockMvc = MockMvcBuilders.standaloneSetup(pathResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Path createEntity(EntityManager em) {
        Path path = new Path()
            .type(DEFAULT_TYPE)
            .path(DEFAULT_PATH);
        return path;
    }

    @Before
    public void initTest() {
        path = createEntity(em);
    }

    @Test
    @Transactional
    public void createPath() throws Exception {
        int databaseSizeBeforeCreate = pathRepository.findAll().size();

        // Create the Path
        PathDTO pathDTO = pathMapper.toDto(path);
        restPathMockMvc.perform(post("/api/paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathDTO)))
            .andExpect(status().isCreated());

        // Validate the Path in the database
        List<Path> pathList = pathRepository.findAll();
        assertThat(pathList).hasSize(databaseSizeBeforeCreate + 1);
        Path testPath = pathList.get(pathList.size() - 1);
        assertThat(testPath.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testPath.getPath()).isEqualTo(DEFAULT_PATH);
    }

    @Test
    @Transactional
    public void createPathWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = pathRepository.findAll().size();

        // Create the Path with an existing ID
        path.setId(1L);
        PathDTO pathDTO = pathMapper.toDto(path);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPathMockMvc.perform(post("/api/paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Path in the database
        List<Path> pathList = pathRepository.findAll();
        assertThat(pathList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = pathRepository.findAll().size();
        // set the field null
        path.setType(null);

        // Create the Path, which fails.
        PathDTO pathDTO = pathMapper.toDto(path);

        restPathMockMvc.perform(post("/api/paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathDTO)))
            .andExpect(status().isBadRequest());

        List<Path> pathList = pathRepository.findAll();
        assertThat(pathList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPathIsRequired() throws Exception {
        int databaseSizeBeforeTest = pathRepository.findAll().size();
        // set the field null
        path.setPath(null);

        // Create the Path, which fails.
        PathDTO pathDTO = pathMapper.toDto(path);

        restPathMockMvc.perform(post("/api/paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathDTO)))
            .andExpect(status().isBadRequest());

        List<Path> pathList = pathRepository.findAll();
        assertThat(pathList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPaths() throws Exception {
        // Initialize the database
        pathRepository.saveAndFlush(path);

        // Get all the pathList
        restPathMockMvc.perform(get("/api/paths?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(path.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH.toString())));
    }
    
    @Test
    @Transactional
    public void getPath() throws Exception {
        // Initialize the database
        pathRepository.saveAndFlush(path);

        // Get the path
        restPathMockMvc.perform(get("/api/paths/{id}", path.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(path.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingPath() throws Exception {
        // Get the path
        restPathMockMvc.perform(get("/api/paths/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePath() throws Exception {
        // Initialize the database
        pathRepository.saveAndFlush(path);

        int databaseSizeBeforeUpdate = pathRepository.findAll().size();

        // Update the path
        Path updatedPath = pathRepository.findById(path.getId()).get();
        // Disconnect from session so that the updates on updatedPath are not directly saved in db
        em.detach(updatedPath);
        updatedPath
            .type(UPDATED_TYPE)
            .path(UPDATED_PATH);
        PathDTO pathDTO = pathMapper.toDto(updatedPath);

        restPathMockMvc.perform(put("/api/paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathDTO)))
            .andExpect(status().isOk());

        // Validate the Path in the database
        List<Path> pathList = pathRepository.findAll();
        assertThat(pathList).hasSize(databaseSizeBeforeUpdate);
        Path testPath = pathList.get(pathList.size() - 1);
        assertThat(testPath.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testPath.getPath()).isEqualTo(UPDATED_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingPath() throws Exception {
        int databaseSizeBeforeUpdate = pathRepository.findAll().size();

        // Create the Path
        PathDTO pathDTO = pathMapper.toDto(path);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPathMockMvc.perform(put("/api/paths")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(pathDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Path in the database
        List<Path> pathList = pathRepository.findAll();
        assertThat(pathList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePath() throws Exception {
        // Initialize the database
        pathRepository.saveAndFlush(path);

        int databaseSizeBeforeDelete = pathRepository.findAll().size();

        // Get the path
        restPathMockMvc.perform(delete("/api/paths/{id}", path.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Path> pathList = pathRepository.findAll();
        assertThat(pathList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Path.class);
        Path path1 = new Path();
        path1.setId(1L);
        Path path2 = new Path();
        path2.setId(path1.getId());
        assertThat(path1).isEqualTo(path2);
        path2.setId(2L);
        assertThat(path1).isNotEqualTo(path2);
        path1.setId(null);
        assertThat(path1).isNotEqualTo(path2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PathDTO.class);
        PathDTO pathDTO1 = new PathDTO();
        pathDTO1.setId(1L);
        PathDTO pathDTO2 = new PathDTO();
        assertThat(pathDTO1).isNotEqualTo(pathDTO2);
        pathDTO2.setId(pathDTO1.getId());
        assertThat(pathDTO1).isEqualTo(pathDTO2);
        pathDTO2.setId(2L);
        assertThat(pathDTO1).isNotEqualTo(pathDTO2);
        pathDTO1.setId(null);
        assertThat(pathDTO1).isNotEqualTo(pathDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(pathMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(pathMapper.fromId(null)).isNull();
    }
}
