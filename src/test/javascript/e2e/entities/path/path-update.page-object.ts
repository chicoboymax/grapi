import { element, by, ElementFinder } from 'protractor';

export default class PathUpdatePage {
  pageTitle: ElementFinder = element(by.id('grapiApp.path.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  typeSelect: ElementFinder = element(by.css('select#path-type'));
  pathInput: ElementFinder = element(by.css('input#path-path'));
  openApiSelect: ElementFinder = element(by.css('select#path-openApi'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setTypeSelect(type) {
    await this.typeSelect.sendKeys(type);
  }

  async getTypeSelect() {
    return this.typeSelect.element(by.css('option:checked')).getText();
  }

  async typeSelectLastOption() {
    await this.typeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }
  async setPathInput(path) {
    await this.pathInput.sendKeys(path);
  }

  async getPathInput() {
    return this.pathInput.getAttribute('value');
  }

  async openApiSelectLastOption() {
    await this.openApiSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async openApiSelectOption(option) {
    await this.openApiSelect.sendKeys(option);
  }

  getOpenApiSelect() {
    return this.openApiSelect;
  }

  async getOpenApiSelectedOption() {
    return this.openApiSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
