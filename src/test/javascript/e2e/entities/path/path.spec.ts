/* tslint:disable no-unused-expression */
import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import PathComponentsPage from './path.page-object';
import { PathDeleteDialog } from './path.page-object';
import PathUpdatePage from './path-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('Path e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let pathUpdatePage: PathUpdatePage;
  let pathComponentsPage: PathComponentsPage;
  let pathDeleteDialog: PathDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();

    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load Paths', async () => {
    await navBarPage.getEntityPage('path');
    pathComponentsPage = new PathComponentsPage();
    expect(await pathComponentsPage.getTitle().getText()).to.match(/Paths/);
  });

  it('should load create Path page', async () => {
    await pathComponentsPage.clickOnCreateButton();
    pathUpdatePage = new PathUpdatePage();
    expect(await pathUpdatePage.getPageTitle().getAttribute('id')).to.match(/grapiApp.path.home.createOrEditLabel/);
  });

  it('should create and save Paths', async () => {
    const nbButtonsBeforeCreate = await pathComponentsPage.countDeleteButtons();

    await pathUpdatePage.typeSelectLastOption();
    await pathUpdatePage.setPathInput('path');
    expect(await pathUpdatePage.getPathInput()).to.match(/path/);
    await pathUpdatePage.openApiSelectLastOption();
    await waitUntilDisplayed(pathUpdatePage.getSaveButton());
    await pathUpdatePage.save();
    await waitUntilHidden(pathUpdatePage.getSaveButton());
    expect(await pathUpdatePage.getSaveButton().isPresent()).to.be.false;

    await pathComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await pathComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last Path', async () => {
    await pathComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await pathComponentsPage.countDeleteButtons();
    await pathComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    pathDeleteDialog = new PathDeleteDialog();
    expect(await pathDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/grapiApp.path.delete.question/);
    await pathDeleteDialog.clickOnConfirmButton();

    await pathComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await pathComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
