/* tslint:disable no-unused-expression */
import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import OpenApiComponentsPage from './open-api.page-object';
import { OpenApiDeleteDialog } from './open-api.page-object';
import OpenApiUpdatePage from './open-api-update.page-object';
import { waitUntilDisplayed, waitUntilHidden } from '../../util/utils';

const expect = chai.expect;

describe('OpenApi e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let openApiUpdatePage: OpenApiUpdatePage;
  let openApiComponentsPage: OpenApiComponentsPage;
  let openApiDeleteDialog: OpenApiDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();

    await waitUntilDisplayed(navBarPage.entityMenu);
  });

  it('should load OpenApis', async () => {
    await navBarPage.getEntityPage('open-api');
    openApiComponentsPage = new OpenApiComponentsPage();
    expect(await openApiComponentsPage.getTitle().getText()).to.match(/Open Apis/);
  });

  it('should load create OpenApi page', async () => {
    await openApiComponentsPage.clickOnCreateButton();
    openApiUpdatePage = new OpenApiUpdatePage();
    expect(await openApiUpdatePage.getPageTitle().getAttribute('id')).to.match(/grapiApp.openApi.home.createOrEditLabel/);
  });

  it('should create and save OpenApis', async () => {
    const nbButtonsBeforeCreate = await openApiComponentsPage.countDeleteButtons();

    await openApiUpdatePage.setTitleInput('title');
    expect(await openApiUpdatePage.getTitleInput()).to.match(/title/);
    await openApiUpdatePage.setVersionInput('version');
    expect(await openApiUpdatePage.getVersionInput()).to.match(/version/);
    await openApiUpdatePage.setUriInput('uri');
    expect(await openApiUpdatePage.getUriInput()).to.match(/uri/);
    await openApiUpdatePage.ownerSelectLastOption();
    await waitUntilDisplayed(openApiUpdatePage.getSaveButton());
    await openApiUpdatePage.save();
    await waitUntilHidden(openApiUpdatePage.getSaveButton());
    expect(await openApiUpdatePage.getSaveButton().isPresent()).to.be.false;

    await openApiComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeCreate + 1);
    expect(await openApiComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
  });

  it('should delete last OpenApi', async () => {
    await openApiComponentsPage.waitUntilLoaded();
    const nbButtonsBeforeDelete = await openApiComponentsPage.countDeleteButtons();
    await openApiComponentsPage.clickOnLastDeleteButton();

    const deleteModal = element(by.className('modal'));
    await waitUntilDisplayed(deleteModal);

    openApiDeleteDialog = new OpenApiDeleteDialog();
    expect(await openApiDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/grapiApp.openApi.delete.question/);
    await openApiDeleteDialog.clickOnConfirmButton();

    await openApiComponentsPage.waitUntilDeleteButtonsLength(nbButtonsBeforeDelete - 1);
    expect(await openApiComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
