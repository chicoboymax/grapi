import { element, by, ElementFinder } from 'protractor';

export default class OpenApiUpdatePage {
  pageTitle: ElementFinder = element(by.id('grapiApp.openApi.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  titleInput: ElementFinder = element(by.css('input#open-api-title'));
  versionInput: ElementFinder = element(by.css('input#open-api-version'));
  uriInput: ElementFinder = element(by.css('input#open-api-uri'));
  ownerSelect: ElementFinder = element(by.css('select#open-api-owner'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setTitleInput(title) {
    await this.titleInput.sendKeys(title);
  }

  async getTitleInput() {
    return this.titleInput.getAttribute('value');
  }

  async setVersionInput(version) {
    await this.versionInput.sendKeys(version);
  }

  async getVersionInput() {
    return this.versionInput.getAttribute('value');
  }

  async setUriInput(uri) {
    await this.uriInput.sendKeys(uri);
  }

  async getUriInput() {
    return this.uriInput.getAttribute('value');
  }

  async ownerSelectLastOption() {
    await this.ownerSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async ownerSelectOption(option) {
    await this.ownerSelect.sendKeys(option);
  }

  getOwnerSelect() {
    return this.ownerSelect;
  }

  async getOwnerSelectedOption() {
    return this.ownerSelect.element(by.css('option:checked')).getText();
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
